# RemoveLatexTags

Tool to remove tags from Latex documents.

## Build
```
cabal sandbox init
cabal install -j
```

## Test
```
cabal install --enable-tests
cabal exec -- runhaskell -isrc -itest test/Spec.hs
```