{-# LANGUAGE OverloadedStrings #-}
module RemoveLatexTags (removeTags) where

import Prelude hiding (head)
import Text.Megaparsec (Parsec, (<|>), try, eof, tokens, takeP, takeWhileP, takeWhile1P)
import Text.Megaparsec.Char (string, string')
import Data.Monoid ((<>))
import Data.Void (Void)
import Data.Text (Text, head)

type Parser = Parsec Void Text Text

tagBody :: Text -> Parser -> Parser
tagBody name body = try (string "\\" *> string' name *> string "{" *> body <* string "}")

braces :: Parser -> Parser
braces body = try (string "{" <> body <> string "}")

comment :: Parser
comment = string "\\%" <|> string "%" <> takeWhileP (Just "LF") (/= '\n')

textOrBraces :: Parser
textOrBraces = 
  (braces textOrBraces <|> comment <|> string "\\}" <|> tokens (/=) "}") <> textOrBraces <|> pure mempty

ignore :: Maybe Text -> Parser
ignore Nothing = fail "nothing to ignore"
ignore (Just input) = string' input

removeTags :: Text -> Maybe Text -> Parser
removeTags name exclude =
  (ignore exclude <|> tagBody name textOrBraces <|> comment <|> text) <> (removeTags name exclude) 
    <|> (const mempty <$> eof)
  where
    notFirst Nothing _ = True
    notFirst (Just "") _ = True
    notFirst (Just x) c = head x /= c

    firstExclude = notFirst exclude
    
    text = takeWhile1P (Just "text") (\c -> c /= '\\' && c /= '%' && firstExclude c) <|> takeP (Just "any char") 1

