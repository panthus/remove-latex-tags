module Main where

import Prelude hiding (readFile, putStrLn, getContents, writeFile)
import RemoveLatexTags (removeTags)
import System.Directory (doesDirectoryExist, listDirectory, getCurrentDirectory)
import System.Environment (getArgs)
import System.FilePath ((</>), makeRelative, isRelative, takeExtension)
import Text.Megaparsec (Parsec, parse, (<|>), eof, takeWhile1P)
import Text.Megaparsec.Char (string', space, char)
import Text.Megaparsec.Error (parseErrorPretty)
import Data.Text.IO (putStrLn, readFile, hPutStrLn, getContents, writeFile)
import Data.Text (Text, pack)
import Data.Void (Void)
import System.IO (stdin, stderr, hReady)
import Data.List (intercalate)
import Data.Monoid ((<>))
import Data.Maybe (fromMaybe)

main :: IO ()
main = do
  args <- (\a -> "\"" <> intercalate "\"\"" a <> "\"") <$> getArgs
  cwd <- getCurrentDirectory
  hasStdin <- hReady stdin
  case parse (argumentsP hasStdin) "arguments" args of
    Left e -> hPutStrLn stderr $ pack $ parseErrorPretty e <> "in " <> args
    Right Help -> putStrLn help
    Right (RemoveTagsStdin tag ignore) ->
      removeTagsInTarget tag ignore "stdin" getContents putStrLn
    Right (RemoveTags tag filePath ignore) ->
      removeTagsInFile tag ignore cwd filePath
    Right (RemoveTagsRecursive tag filePath ignore) -> do
      files <- listDirectoryRecursive filePath
      let texFiles = filter ((==) ".tex" . takeExtension) files
      mapM_ (removeTagsInFile tag ignore cwd) texFiles

removeTagsInTarget :: Text -> Text -> String -> IO Text -> (Text -> IO ()) -> IO ()
removeTagsInTarget tag ignore targetName readTarget writeTarget = do
  target <- readTarget
  case parse (removeTags tag (Just ignore)) targetName target of
    Left e -> hPutStrLn stderr $ pack $ parseErrorPretty e
    Right a -> writeTarget a

removeTagsInFile :: Text -> Text -> FilePath -> FilePath -> IO ()
removeTagsInFile tag ignore cwd filePath = do
  let absolute = if isRelative filePath then cwd </> filePath else filePath
  let relative = makeRelative cwd absolute
  removeTagsInTarget tag ignore relative (readFile absolute) (writeFile absolute)

listDirectoryRecursive :: FilePath -> IO [FilePath]
listDirectoryRecursive root = step root (pure [])
  where
    step :: FilePath -> IO [FilePath] -> IO [FilePath]
    step filePath foundFiles = do
      found <- foundFiles
      isDir <- doesDirectoryExist filePath
      list <- if isDir then listDirectory filePath else pure []
      let absoluteList = (\i -> filePath </> i) <$> list
      foldr step (pure $ found <> absoluteList) absoluteList

data Arguments
  -- Help is also used if the other option does not match.
  = Help
  -- Tag, dir path, ignore
  | RemoveTagsRecursive Text FilePath Text
  -- Tag, file path, ignore
  | RemoveTags Text FilePath Text
  -- Tag, ignore
  | RemoveTagsStdin Text Text
  
help :: Text
help =
  "Usage:\n\
  \\tRemoveLatexTags [FILE|DIR] [OPTIONS]\n\
  \\n\
  \Removes tags (\\tag{...}) from latex files.\n\
  \\n\
  \FILE reads a file and outputs to that same FILE.\n\
  \DIR requires recursive (-r) and outputs to the .tex files it finds.\n\
  \STDIN outputs to STDOUT.\n\
  \\n\
  \Arguments:\n\
  \  -h, --help\t\tDisplay this help text.\n\
  \  -r, --recursive\tRemove tags in all .tex documents recursively\n\
  \  \t\t\tfound from the current working directory or DIR.\n\
  \  -t, --tag\t\tSpecify the tag to remove, default 'NEW'.\n\
  \  -i, --ignore\t\tSpecify file content to ignore during remove, default\n\
  \  \t\t\t'\\NEW{Marked text}        & Text has changed compared to the\n\
  \  \t\t\tprevious release.\\\\'."

data RemoveTagsData = RemoveTagsData
  { recursive :: Bool
  , tag :: Maybe Text
  , filePath :: Maybe FilePath
  , ignore :: Maybe Text
  , hasStdin :: Bool
  }

initRemoveTagsData :: RemoveTagsData
initRemoveTagsData = 
  RemoveTagsData { recursive = False, tag = Nothing, filePath = Nothing, ignore = Nothing, hasStdin = False }

helpP :: Parsec Void String Arguments
helpP = const Help <$> (string' "\"-h\"" <|> string' "\"--help\"")

recursiveP :: RemoveTagsData -> Parsec Void String RemoveTagsData
recursiveP d = const (d { recursive = True }) <$> (string' "\"-r\"" <|> string' "\"--recursive\"")

textP :: String -> Parsec Void String String
textP hint =
  char '"' *> takeWhile1P (Just hint) (/= '"') <* char '"'

tagP :: RemoveTagsData -> Parsec Void String RemoveTagsData
tagP d = (\t -> d { tag = Just $ pack t }) 
  <$> ((string' "\"-t\"\"" <|> string' "\"--tag\"\"") *> textP "tag")

filePathP :: RemoveTagsData -> Parsec Void String RemoveTagsData
filePathP d = (\f -> d { filePath = Just f }) <$> textP "file path"

ignoreP :: RemoveTagsData -> Parsec Void String RemoveTagsData
ignoreP d = (\i -> d { ignore = Just $ pack i }) 
  <$> ((string' "\"-i\"" <|> string' "\"--ignore\"") *> textP "ignore")

removeTagsP :: RemoveTagsData -> Parsec Void String RemoveTagsData
removeTagsP d = space *> ((recursiveP d <|> tagP d <|> ignoreP d <|> filePathP d) >>= removeTagsP) <|> (const d <$> eof)

argumentsP :: Bool -> Parsec Void String Arguments
argumentsP hasStdin = helpP <|> (validate =<< (\d -> d { hasStdin = hasStdin }) <$> removeTagsP initRemoveTagsData) 
  <|> pure Help

validate :: RemoveTagsData -> Parsec Void String Arguments
validate d =
  case d of
    RemoveTagsData { recursive = False, filePath = Just f, tag = t, ignore = i, hasStdin = False } ->
      pure $ RemoveTags (tagWithDefault t) f (ignoreWithDefault i)
    RemoveTagsData { recursive = True, filePath = f, tag = t, ignore = i, hasStdin = False } ->
      pure $ RemoveTagsRecursive (tagWithDefault t) (filePathWithDefault f) (ignoreWithDefault i)
    RemoveTagsData { recursive = False, filePath = Nothing, tag = t, ignore = i, hasStdin = True } ->
      pure $ RemoveTagsStdin (tagWithDefault t) (ignoreWithDefault i)
    _ ->
      fail "Provide either a FILE, STDIN or recursive (-r) with optional DIR option."
  where
    tagWithDefault = fromMaybe "NEW"
    ignoreWithDefault = fromMaybe "\\NEW{Marked text}        & Text has changed compared to the previous release.\\\\"
    filePathWithDefault = fromMaybe "."