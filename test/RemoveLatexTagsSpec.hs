{-# LANGUAGE OverloadedStrings #-}
module RemoveLatexTagsSpec where
  
import RemoveLatexTags
import Test.Hspec
import Text.Megaparsec
import Data.Text

testParser' :: Maybe Text -> Text -> Text -> Expectation
testParser' ignore contents parsedContents =
  shouldBe
    (parse (removeTags "NEW" ignore) "test" contents)
    (Right parsedContents)

testParser :: Text -> Text -> Expectation
testParser = testParser' Nothing

spec :: Spec
spec =
  describe "Removing latex new tags" $ do
    it "should remove new tags" $
      testParser "bla \\new{text} bla" "bla text bla"
    it "should remove empty new tags" $
      testParser "bla \\new{} bla" "bla  bla"
    it "should be case insensitive" $
      testParser "\\New{bla} \\NEW{text} \\new{bla}" "bla text bla"
    it "should ignore other tags" $
      testParser "b\\la \\texttt{text} \\new{bla}" "b\\la \\texttt{text} bla"
    it "should handle nested new tags" $
      testParser 
        "\\new{some \\other{other \\more{taginess}} \\tag{more}}"
        "some \\other{other \\more{taginess}} \\tag{more}"
    it "should handle tags wrapped in braces" $
      testParser "some \\new{some {\\other{other}}}" "some some {\\other{other}}"
    it "should handle empty input" $
      testParser "" ""
    it "should handle escaped braces, procent and only excape" $
      testParser "\\new{text\\}tex\\%t}\\ \\{" "text\\}tex\\%t\\ \\{"
    it "should handle commented tags" $
      testParser 
        "first line\n\
          \%some \\new{some {\\other{other}}}\r\n\
          \\\new{bla}" 
        "first line\n\
          \%some \\new{some {\\other{other}}}\r\n\
          \bla" 
    it "should handle nested commented tags" $
      testParser 
        "first \\new{line\n\
          \bla\n\
          \%some \\new{some {\\other{other}}}\r\n\
          \}\\new{bla}" 
        "first line\n\
          \bla\n\
          \%some \\new{some {\\other{other}}}\r\n\
          \bla" 
    it "should handle ignored input" $
      testParser'
        (Just "\\NEW{NEW} New since last release.")
        "bla\n\
          \\\new{NEW} New since last release.\n\
          \\\new{some}"
        "bla\n\
          \\\new{NEW} New since last release.\n\
          \some"