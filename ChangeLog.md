# Revision history for RemoveLatexTags

## 0.1.0.0  -- 2017-12-10

* First version. Supports removing new tags from a specified file.
